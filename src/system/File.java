package system;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 3:54 PM
 * To change this template use system.File | Settings | system.File Templates.
 */

public class File {

    private String name;
    private Date createdTime;
    private String content;

    public File(String name, String content) {
        this.name = name;
        this.createdTime = new Date();
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
