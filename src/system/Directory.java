package system;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 3:54 PM
 * To change this template use system.File | Settings | system.File Templates.
 */
public class Directory {

    private String path;
    private String name;
    private List<File> files = null;
    private List<Directory> directories = null;

    public Directory() {

    }

    public Directory(String path) {
        this.path = path;
    }

    public File createFile(String name, String content) {
        if(files == null) {
            files = new ArrayList<File>();
        }

        File file = new File(name, content);
        files.add(file);
        return file;
    }

    public Directory createDirectory(String path) {
        if(directories == null) {
            directories = new ArrayList<Directory>();
        }

        Directory directory = new Directory(path);
        directories.add(directory);
        return directory;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Directory> getDirectories() {
        return directories;
    }

    public void setDirectories(List<Directory> directories) {
        this.directories = directories;
    }
}
