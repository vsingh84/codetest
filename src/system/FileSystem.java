package system;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 4:06 PM
 * To change this template use system.File | Settings | system.File Templates.
 */
public class FileSystem extends Directory {

    private List<Directory> directories;

    public List<Directory> getDirectories() {
        return directories;
    }

    public void setDirectories(List<Directory> directories) {
        this.directories = directories;
    }
}
