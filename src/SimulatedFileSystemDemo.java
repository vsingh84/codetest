import commands.Command;
import commands.CommandFactory;
import system.Directory;
import system.FileSystem;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 11:35 AM
 * To change this template use system.File | Settings | system.File Templates.
 */
public class SimulatedFileSystemDemo {

    public static void main(String[] args) {
        FileSystem fileSystem = new FileSystem();

        Directory testDir = fileSystem.createDirectory("/loyal3/test");
        testDir.createFile("test.txt", "This is the first test file");
        testDir.createFile("test2.txt", "This is the second test file");

        Command lsCommand = CommandFactory.getCommand("ls");
        lsCommand.executeCommand(fileSystem, fileSystem.getPath(), "");


    }
}
