package commands;


import system.FileSystem;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 4:15 PM
 * To change this template use system.File | Settings | system.File Templates.
 */
public interface Command {

    public void executeCommand(FileSystem fileSystem, String path, String param);
}
