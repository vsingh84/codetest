package commands;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 4:11 PM
 * To change this template use system.File | Settings | system.File Templates.
 */
public class CommandFactory {

   public static Command getCommand(String command) {
       if(command.equalsIgnoreCase("ls")) {
           return new ListCommand();
       } else if(command.equalsIgnoreCase("mkdir")) {
           return new MakeDirectoryCommand();
       } else if(command.equalsIgnoreCase("cd")) {
           return new ChangeDirectoryCommand();
       } else if(command.equalsIgnoreCase("touch")) {
           return new TouchCommand();
       }
       return null;
   }
}
