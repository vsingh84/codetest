package commands;

import system.Directory;
import system.File;
import system.FileSystem;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 4:17 PM
 * To change this template use system.File | Settings | system.File Templates.
 */
public class ListCommand implements Command {

    public void executeCommand(FileSystem fileSystem, String path, String param) {
        for(Directory dir : fileSystem.getDirectories()) {
            if(path.equalsIgnoreCase(dir.getPath())) {
                for(File file : dir.getFiles())
                System.out.print("File: " + file.getName());
            }
            System.out.print("Directory: " + dir.getName());
        }
    }
}
