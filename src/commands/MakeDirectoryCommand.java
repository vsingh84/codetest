package commands;


import system.FileSystem;

/**
 * Created with IntelliJ IDEA.
 * User: vsingh
 * Date: 3/25/15
 * Time: 4:18 PM
 * To change this template use system.File | Settings | system.File Templates.
 */
public class MakeDirectoryCommand implements Command {

    public void executeCommand(FileSystem fileSystem, String path, String param) {
        /**
         * Take a path and name as a parameter and go through the file system to search for the path.
         *
         * If the path exists do nothing.
         *
         * If the path does not exist use the createDirectory method
         *
         * Need to check if part of the path exists.
         */
    }
}
